import sys
import time
from binance_requests import BinanceClient

def main():
    client = BinanceClient(secretFilePath=sys.argv[1])
    while True:
        req = client.getAllOpenOrders()
        buys = list(filter(lambda x : x["side"] == "BUY", req))
        sells = list(filter(lambda x : x["side"] == "SELL", req))
        totalBuy, totalSell = 0, 0
        print("[Open orders] Buys: %d | Sells: %d" % (len(buys), len(sells)))
        print("[Buys]")
        for item in buys:
            symbol, quantity, price = item["symbol"], float(item["origQty"]), float(item["price"])
            totalBuy += quantity * price
            print(" - Symbol: %s. Quantity: %d. Price: %2.5f" % (symbol, quantity, price))
        print(" -> Total: %2.2f BUSD" % totalBuy)

        print("\n[Sells]")
        for item in sells:
            symbol, quantity, price = item["symbol"], float(item["origQty"]), float(item["price"])
            totalSell += quantity * price
            print(" - Symbol: %s. Quantity: %d. Price: %2.5f" % (symbol, quantity, price))
        print(" -> Total: %2.2f BUSD" % totalSell)

        print("___________________")
        time.sleep(1)

if __name__ == "__main__":
    main()