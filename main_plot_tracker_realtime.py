import sys
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from binance_requests import BinanceClient
from typing import List
from utils import doPlot, processOneCoin

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--coins", required=True)
    parser.add_argument("--mode", default="show")
    parser.add_argument("--maxNumValues", type=int, default=1<<31)
    parser.add_argument("--savePath")
    args = parser.parse_args()
    args.coins = [("%sBUSD" % x).upper() for x in args.coins.split(",")]
    assert args.mode in ("save", "show")
    if args.mode == "save":
        assert not args.savePath is None
    return args

def balanceGenerator(client:BinanceClient, coins:List[str], maxNumValues:int):
    coinsStats = {}
    while True:
        try:
            req = client.getAllPrices()
        except Exception as e:
            print(str(e))
            continue

        reqCoins = {coin : req[coin] for coin in coins}
        validThisIter = []
        for coin in reqCoins:
            if not coin in coinsStats:
                coinsStats[coin] = []

            coinsStats[coin] = processOneCoin(coinsStats[coin], reqCoins[coin], maxNumValues)
            validThisIter.append(coin)
        yield {k : coinsStats[k] for k in validThisIter}

def main():
    args = getArgs()
    client = BinanceClient(secretFilePath=None)
    g = balanceGenerator(client, args.coins, args.maxNumValues)

    while True:
        try:
            res = next(g)
        except Exception as e:
            print(str(e))
            breakpoint()
            continue
        doPlot(res)

        if args.mode == "show":
            plt.pause(0.05)
        else:
            plt.savefig(args.savePath)

if __name__ == "__main__":
    main()